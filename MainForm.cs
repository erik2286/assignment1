﻿/* MainForm.cs
 * Assignment1
 * Erik Altynbayev*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Channels;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EAltynbayevAssignment1
{
    /// <summary>
    /// Makes all the work: creates and sustains the game
    /// </summary>
    public partial class MainForm : Form
    {
        private Image imgX = EAltynbayevAssignment1.Properties.Resources.x;
        private Image imgO = EAltynbayevAssignment1.Properties.Resources.o;
        bool some = true; //true == x turn; false == o turn
        int turn = 0;

        /// <summary>
        /// Default constructor of the MainForm class
        /// </summary>
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// checks after every turn if someone has won
        /// </summary>
        /// <returns>false(the game keeps going) or true(someone has won)</returns>
        bool Check()
        {
            if ((picBox1.Image == picBox2.Image) && (picBox2.Image == picBox3.Image) && ((picBox1.Image == imgX) || (picBox1.Image == imgO)))
                return true;
            else if ((picBox4.Image == picBox5.Image) && (picBox5.Image == picBox6.Image) && ((picBox4.Image == imgX) || (picBox4.Image == imgO)))
                return true;
            else if ((picBox7.Image == picBox8.Image) && (picBox8.Image == picBox9.Image) && ((picBox7.Image == imgX) || (picBox7.Image == imgO)))
                return true;

            if ((picBox1.Image == picBox4.Image) && (picBox4.Image == picBox7.Image) && ((picBox1.Image == imgX) || (picBox1.Image == imgO)))
                return true;
            else if ((picBox2.Image == picBox5.Image) && (picBox5.Image == picBox8.Image) && ((picBox2.Image == imgX) || (picBox2.Image == imgO)))
                return true;
            else if ((picBox3.Image == picBox6.Image) && (picBox6.Image == picBox9.Image) && ((picBox3.Image == imgX) || (picBox3.Image == imgO)))
                return true;

            if ((picBox1.Image == picBox5.Image) && (picBox5.Image == picBox9.Image) && ((picBox1.Image == imgX) || (picBox1.Image == imgO)))
                return true;
            else if ((picBox3.Image == picBox5.Image) && (picBox5.Image == picBox7.Image) && ((picBox3.Image == imgX) || (picBox3.Image == imgO)))
                return true;
            else
            {
                return false;
            }
        }

        /// <summary>
        /// ends a game and starts a new one
        /// </summary>
        void End()
        {
            picBox1.Image = picBox2.Image = picBox3.Image = picBox4.Image =
            picBox5.Image = picBox6.Image = picBox7.Image = picBox8.Image =
            picBox9.Image = null;
            picBox1.Enabled = true;
            picBox2.Enabled = true;
            picBox3.Enabled = true;
            picBox4.Enabled = true;
            picBox5.Enabled = true;
            picBox6.Enabled = true;
            picBox7.Enabled = true;
            picBox8.Enabled = true;
            picBox9.Enabled = true;
            turn = 0;
            some = true;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            PictureBox picBox = (PictureBox)sender;
            {
                turn++;
                picBox.Enabled = false;
                if (some == true)
                {
                    picBox.Image = imgX;
                    some = false;
                    if (Check() == false)
                    {

                    }
                    else
                    {
                        var result = MessageBox.Show("X wins", "Tic Tac Toe");
                        if (result == DialogResult.OK)
                        {
                            End();
                        }
                    }

                }
                else
                {
                    picBox.Image = imgO;
                    some = true;
                    if (Check() == false)
                    {

                    }
                    else
                    {
                        var result = MessageBox.Show("O wins", "Tic Tac Toe");
                        if (result == DialogResult.OK)
                        {
                            End();
                        }
                    }

                }
                if (turn == 9)
                {
                    var result = MessageBox.Show("Draw", "Tic Tac Toe");
                    if (result == DialogResult.OK)
                    {
                        End();
                    }
                }
            }

        }
    }
}
