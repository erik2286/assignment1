﻿namespace EAltynbayevAssignment1
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picBox1 = new System.Windows.Forms.PictureBox();
            this.picBox9 = new System.Windows.Forms.PictureBox();
            this.picBox8 = new System.Windows.Forms.PictureBox();
            this.picBox4 = new System.Windows.Forms.PictureBox();
            this.picBox5 = new System.Windows.Forms.PictureBox();
            this.picBox6 = new System.Windows.Forms.PictureBox();
            this.picBox7 = new System.Windows.Forms.PictureBox();
            this.picBox3 = new System.Windows.Forms.PictureBox();
            this.picBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // picBox1
            // 
            this.picBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox1.Location = new System.Drawing.Point(52, 40);
            this.picBox1.Name = "picBox1";
            this.picBox1.Size = new System.Drawing.Size(98, 75);
            this.picBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox1.TabIndex = 0;
            this.picBox1.TabStop = false;
            this.picBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // picBox9
            // 
            this.picBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox9.Location = new System.Drawing.Point(260, 202);
            this.picBox9.Name = "picBox9";
            this.picBox9.Size = new System.Drawing.Size(98, 75);
            this.picBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox9.TabIndex = 1;
            this.picBox9.TabStop = false;
            this.picBox9.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // picBox8
            // 
            this.picBox8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox8.Location = new System.Drawing.Point(156, 202);
            this.picBox8.Name = "picBox8";
            this.picBox8.Size = new System.Drawing.Size(98, 75);
            this.picBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox8.TabIndex = 2;
            this.picBox8.TabStop = false;
            this.picBox8.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // picBox4
            // 
            this.picBox4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox4.Location = new System.Drawing.Point(52, 121);
            this.picBox4.Name = "picBox4";
            this.picBox4.Size = new System.Drawing.Size(98, 75);
            this.picBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox4.TabIndex = 3;
            this.picBox4.TabStop = false;
            this.picBox4.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // picBox5
            // 
            this.picBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox5.Location = new System.Drawing.Point(156, 121);
            this.picBox5.Name = "picBox5";
            this.picBox5.Size = new System.Drawing.Size(98, 75);
            this.picBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox5.TabIndex = 4;
            this.picBox5.TabStop = false;
            this.picBox5.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // picBox6
            // 
            this.picBox6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox6.Location = new System.Drawing.Point(260, 121);
            this.picBox6.Name = "picBox6";
            this.picBox6.Size = new System.Drawing.Size(98, 75);
            this.picBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox6.TabIndex = 5;
            this.picBox6.TabStop = false;
            this.picBox6.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // picBox7
            // 
            this.picBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox7.Location = new System.Drawing.Point(52, 202);
            this.picBox7.Name = "picBox7";
            this.picBox7.Size = new System.Drawing.Size(98, 75);
            this.picBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox7.TabIndex = 6;
            this.picBox7.TabStop = false;
            this.picBox7.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // picBox3
            // 
            this.picBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox3.Location = new System.Drawing.Point(260, 40);
            this.picBox3.Name = "picBox3";
            this.picBox3.Size = new System.Drawing.Size(98, 75);
            this.picBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox3.TabIndex = 7;
            this.picBox3.TabStop = false;
            this.picBox3.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // picBox2
            // 
            this.picBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.picBox2.Location = new System.Drawing.Point(156, 40);
            this.picBox2.Name = "picBox2";
            this.picBox2.Size = new System.Drawing.Size(98, 75);
            this.picBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBox2.TabIndex = 8;
            this.picBox2.TabStop = false;
            this.picBox2.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(434, 331);
            this.Controls.Add(this.picBox2);
            this.Controls.Add(this.picBox3);
            this.Controls.Add(this.picBox7);
            this.Controls.Add(this.picBox6);
            this.Controls.Add(this.picBox5);
            this.Controls.Add(this.picBox4);
            this.Controls.Add(this.picBox8);
            this.Controls.Add(this.picBox9);
            this.Controls.Add(this.picBox1);
            this.Name = "MainForm";
            this.Text = "Tic Tac Toe";
            ((System.ComponentModel.ISupportInitialize)(this.picBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBox2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox picBox1;
        private System.Windows.Forms.PictureBox picBox9;
        private System.Windows.Forms.PictureBox picBox8;
        private System.Windows.Forms.PictureBox picBox4;
        private System.Windows.Forms.PictureBox picBox5;
        private System.Windows.Forms.PictureBox picBox6;
        private System.Windows.Forms.PictureBox picBox7;
        private System.Windows.Forms.PictureBox picBox3;
        private System.Windows.Forms.PictureBox picBox2;
    }
}

